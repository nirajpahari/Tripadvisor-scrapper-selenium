import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
import time
import json
import argparse


def get_reviews(soup, ho):
    reviews = soup.find_all("div", class_="reviewSelector")
    count = 0
    for review in reviews:
        review_object = {}
        review_id = review['data-reviewid']
        review_date = review.find("span", class_="ratingDate")['title']

        review_score = review.find("span", class_="ui_bubble_rating")['class']
        review_score = review_score[1].replace("bubble_", "")
        review_score = ".".join(review_score)

        reviewer_name = review.find("div", class_="info_text").find("div").get_text()
        try:
            reviewer_address = review.find("div", class_="userLoc").get_text()
        except AttributeError:
            reviewer_address = None

        reviewer_attributes = review.find_all("span", class_="badgetext")
        try:
            reviewer_contibution = reviewer_attributes[0].get_text()
        except IndexError:
            reviewer_contibution = None
        try:
            reviewer_helpful_votes = reviewer_attributes[1].get_text()
        except IndexError:
            reviewer_helpful_votes = None
        review_title = review.find("span", class_="noQuotes").get_text()
        review_text = review.find("div", class_="prw_reviews_text_summary_hsx").get_text()
        try:
            reviewer_stayed = review.find("div", class_="recommend-titleInline noRatings").get_text()
            reviewer_stayed = reviewer_stayed.split(", ")
            try:
                stayed = reviewer_stayed[0].replace("Stayed: ", "")
            except IndexError:
                stayed = None
            try:
                travelled_as = reviewer_stayed[1]
            except IndexError:
                travelled_as = None
        except AttributeError:
            stayed = None
            travelled_as = None
            reviewer_stayed = None

        review_object['review_id'] = review_id
        review_object['review_date'] = review_date
        review_object['review_score'] = review_score
        review_object['reviewer_name'] = reviewer_name
        review_object['reviewer_address'] = reviewer_address
        review_object['reviewer_contribution'] = reviewer_contibution
        review_object['reviewer_helpful_votes'] = reviewer_helpful_votes
        review_object['review_text'] = review_text.replace("Show less", "")
        review_object['review_title'] = review_title
        review_object['reviewer_stayed'] = stayed
        review_object['reviewer_travelled_as'] = travelled_as
        ho.append(review_object)
        count+=1
    return ho, count


class GetPage():
    BASE_URL = "https://www.tripadvisor.com"
    HOTEL_URL = "https://www.tripadvisor.com/Hotel_Review-g293891-d5982537-Reviews-Hotel_Mountain_View-Pokhara_Gandaki_Zone_Western_Region.html"

    driver = webdriver.Firefox()
    driver.get(HOTEL_URL)

    more_button = driver.find_element_by_class_name("ulBlueLinks")
    if more_button:
        more_button.click()
        time.sleep(2)

    page_source = driver.page_source

    #
    # # Getting the response form the page
    # page = requests.get(BASE_URL)

    # Initialization of a hotel object
    hotel_object = {}
    hotel_object['reviews'] = []

    # =========================
    # html_file = open("trip_file", 'r')
    # html_file.write(page.content)
    # html_file.close()
    # =========================

    soup = BeautifulSoup(page_source, 'html.parser')

    hotel_name = soup.find(id="HEADING").get_text()
    street_address = soup.find("span", class_="street-address").get_text()
    locality = soup.find("span", class_="locality").get_text()
    country = soup.find("span", class_="country-name").get_text()
    address = ", ".join([street_address, locality, country])
    rating = soup.find("span", property="ratingValue")['content']
    rank = soup.find("b", class_="rank").get_text()
    hotel_object['name'] = hotel_name
    hotel_object['address'] = address
    hotel_object['rating'] = rating
    hotel_object['rank'] = rank.replace("#", "")
    count = 0
    while True:
        hotel_object['reviews'], count_obj = get_reviews(soup, hotel_object['reviews'])
        count += count_obj
        # check the next page here
        next_button = soup.find("a", class_="next")

        if "disabled" not in next_button['class']:
            # get the next page and parse data
            url_part = next_button['href']
            driver.find_element_by_class_name("next").click()
            time.sleep(2)
            more_button = driver.find_element_by_class_name("ulBlueLinks")
            if more_button:
                more_button.click()
                time.sleep(2)
            page_source = driver.page_source
            soup = BeautifulSoup(page_source, 'html.parser')
        else:
            break
    print "-------------------------"
    print hotel_object

    print json.dumps(hotel_object)
    json_file = open("trip_file.json", 'wb')
    json_file.write(json.dumps(hotel_object))
    json_file.close()
    print count
    print "-------------------------"

